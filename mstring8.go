package mstring8

import (
    "unicode/utf8"
)

func MakeRune(data string) []string {
    out := make([]string, 0)
    for _, txt := range data {
        buf := make([]byte, 10)
        n := utf8.EncodeRune(buf, txt)
        out = append(out, string(buf[:n]))
    }
    return out
}

func MakeString(data []string) string {
    out := ""
    for _, txt := range data {
        out = out+txt
    }
    return out
}